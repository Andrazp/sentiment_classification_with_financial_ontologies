## baseline learners

from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn import pipeline
from sklearn.dummy import DummyClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import f1_score
from gensim.utils import simple_preprocess
try:
    import nltk
    nltk.data.path.append("./nltk_data")
except Exception as es:
    import nltk
    print(es)
from sklearn.pipeline import FeatureUnion
from sklearn import pipeline
from sklearn.preprocessing import Normalizer
import numpy as np
from sklearn.metrics import f1_score
import pandas as pd
import logging


logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S')

try:
    from tpot import TPOTClassifier
except:
    print("no tpot")

try:
    from simpletransformers.classification import ClassificationModel

except:
    print("No simpletransformers lib")
try:
    import transformers
except:
    print("No transformers lib")
try:
    from sentence_transformers import SentenceTransformer
except:
    pass
try:
    import logging
except:
    pass

global_svm_range = [0.1, 0.5, 1, 5, 10, 20, 50, 100, 500]

# def BASELINE_get_bert_m2m100_418M_base(train_sequences,
#                   train_targets,
#                   time_constraint=1,
#                   num_cpu=1,
#                   max_features=1000,
#                   model="bert-base",
#                   dev_perc = 0.15):

#     whole_length = len(train_sequences)
#     dev_num = int(whole_length * (1 - dev_perc))

#     dev_sequences = train_sequences[dev_num:]
#     dev_targets = train_targets[dev_num:]

#     train_sequences = train_sequences[0:dev_num]
#     train_targets = train_targets[0:dev_num]

#     total_sequences_training = train_sequences + dev_sequences

#     total_labels_training = train_targets + dev_targets

#     train_df = pd.DataFrame()
#     train_df['text'] = total_sequences_training
#     train_df['labels'] = total_labels_training

#     # Create a ClassificationModel
#     model_args = {}
#     model_args['num_train_epochs'] = 3
#     model_args['max_sequence_length'] = 512
#     model_args['save_eval_checkpoints'] = False
#     model_args['save_model_every_epoch'] = False
#     #model_args['save_steps'] = 4000
#     model = ClassificationModel('bert',
#                                 'facebook/m2m100_418M',
#                                 num_labels=len(set(total_labels_training)),
#                                 args = model_args,
#                                 use_cuda=False)

#     model.train_model(train_df)
#     return model


# def BASELINE_get_bert_mbart_base(train_sequences,
#                   train_targets,
#                   time_constraint=1,
#                   num_cpu=1,
#                   max_features=1000,
#                   model="bert-base",
#                   dev_perc = 0.15):

#     whole_length = len(train_sequences)
#     dev_num = int(whole_length * (1 - dev_perc))

#     dev_sequences = train_sequences[dev_num:]
#     dev_targets = train_targets[dev_num:]

#     train_sequences = train_sequences[0:dev_num]
#     train_targets = train_targets[0:dev_num]

#     total_sequences_training = train_sequences + dev_sequences

#     total_labels_training = train_targets + dev_targets

#     train_df = pd.DataFrame()
#     train_df['text'] = total_sequences_training
#     train_df['labels'] = total_labels_training

#     # Create a ClassificationModel
#     model_args = {}
#     model_args['num_train_epochs'] = 3
#     model_args['max_sequence_length'] = 512
#     model_args['save_eval_checkpoints'] = False
#     model_args['save_model_every_epoch'] = False
#     #model_args['save_steps'] = 4000
#     model = ClassificationModel('bert',
#                                 'facebook/mbart-large-50-one-to-many-mmt',
#                                 num_labels=len(set(total_labels_training)),
#                                 args = model_args,
#                                 use_cuda=False)

#     model.train_model(train_df)
#     return model



def BASELINE_get_svm_char_pipeline(train_sequences,
                                   train_targets,
                                   time_constraint=1,
                                   num_cpu=1,
                                   max_features=1000,
                                   dev_perc=0.15):
    copt = 0
    opt_c = 0

    ## split to train-dev
    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(analyzer='char',
                                     ngram_range=(2, 4),
                                     max_features=max_features)
        clf = LinearSVC(C=c)
        svm_pip = pipeline.Pipeline([('vec', vectorizer),
                                     ('scale', Normalizer()),
                                     ('classifier', clf)])
        svm_pip.fit(train_sequences, train_targets)
        preds = svm_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)
        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1
            opt_c = c
    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(2, 4),
                                 max_features=max_features)
    clf = LinearSVC(C=opt_c)
    svm_pip = pipeline.Pipeline([('vec', vectorizer), ('scale', Normalizer()),
                                 ('classifier', clf)])
    return svm_pip.fit(train_sequences, train_targets)


def BASELINE_get_svm_word_pipeline(train_sequences,
                                   train_targets,
                                   time_constraint=1,
                                   num_cpu=1,
                                   max_features=1000,
                                   dev_perc=0.15):
    copt = 0
    opt_c = 0

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(ngram_range=(1, 3),
                                     max_features=max_features)
        clf = LinearSVC(C=c)
        svm_pip = pipeline.Pipeline([('vec', vectorizer),
                                     ('scale', Normalizer()),
                                     ('classifier', clf)])
        svm_pip.fit(train_sequences, train_targets)
        preds = svm_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)
        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1
            opt_c = c
    vectorizer = TfidfVectorizer(ngram_range=(1, 3), max_features=max_features)
    clf = LinearSVC(C=opt_c)
    svm_pip = pipeline.Pipeline([('vec', vectorizer), ('scale', Normalizer()),
                                 ('classifier', clf)])
    return svm_pip.fit(train_sequences, train_targets)


def BASELINE_get_lr_word_pipeline(train_sequences,
                                  train_targets,
                                  time_constraint=1,
                                  num_cpu=1,
                                  max_features=1000,
                                  dev_perc=0.15):
    copt = 0
    opt_c = 0

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(ngram_range=(1, 3),
                                     max_features=max_features)
        clf = LogisticRegression(C=c, max_iter=100000)
        lr_pip = pipeline.Pipeline([('vec', vectorizer),
                                    ('scale', Normalizer()),
                                    ('classifier', clf)])
        lr_pip.fit(train_sequences, train_targets)
        preds = lr_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)
        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1
            opt_c = c
    vectorizer = TfidfVectorizer(ngram_range=(1, 3), max_features=max_features)
    clf = LinearSVC(C=opt_c)
    lr_pip = pipeline.Pipeline([('vec', vectorizer), ('scale', Normalizer()),
                                ('classifier', clf)])
    return lr_pip.fit(train_sequences, train_targets)


def BASELINE_get_lr_char_pipeline(train_sequences,
                                  train_targets,
                                  time_constraint=1,
                                  num_cpu=1,
                                  max_features=1000,
                                  dev_perc=0.15):
    copt = 0
    opt_c = 0

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(analyzer='char',
                                     ngram_range=(2, 4),
                                     max_features=max_features)
        clf = LogisticRegression(C=c, max_iter=100000)
        lr_pip = pipeline.Pipeline([('vec', vectorizer),
                                    ('scale', Normalizer()),
                                    ('classifier', clf)])
        lr_pip.fit(train_sequences, train_targets)
        preds = lr_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)

        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1
            opt_c = c
    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(2, 4),
                                 max_features=max_features)
    clf = LogisticRegression(C=opt_c, max_iter=1000000)
    lr_pip = pipeline.Pipeline([('vec', vectorizer), ('scale', Normalizer()),
                                ('classifier', clf)])
    return lr_pip.fit(train_sequences, train_targets)


def BASELINE_get_lr_word_char_pipeline(train_sequences,
                                       train_targets,
                                       time_constraint=1,
                                       num_cpu=1,
                                       max_features=1000,
                                       dev_perc=0.15):
    copt = 0
    opt_c = 0

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(analyzer='char',
                                     ngram_range=(2, 4),
                                     max_features=max_features)
        vectorizer2 = TfidfVectorizer(ngram_range=(1, 3),
                                      max_features=max_features)

        features = [('word', vectorizer), ('char', vectorizer2)]
        clf = LogisticRegression(C=c, max_iter=100000)
        lr_pip = pipeline.Pipeline([('union',
                                     FeatureUnion(transformer_list=features)),
                                    ('scale', Normalizer()),
                                    ('classifier', clf)])

        lr_pip.fit(train_sequences, train_targets)
        preds = lr_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)
        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1
            opt_c = c

    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(2, 4),
                                 max_features=max_features)
    vectorizer2 = TfidfVectorizer(ngram_range=(1, 3),
                                  max_features=max_features)
    features = [('word', vectorizer), ('char', vectorizer2)]
    clf = LogisticRegression(C=opt_c, max_iter=100000)
    lr_pip = pipeline.Pipeline([('union',
                                 FeatureUnion(transformer_list=features)),
                                ('scale', Normalizer()), ('classifier', clf)])
    return lr_pip.fit(train_sequences, train_targets)


def BASELINE_get_svm_word_char_pipeline(train_sequences,
                                        train_targets,
                                        time_constraint=1,
                                        num_cpu=1,
                                        max_features=1000,
                                        dev_perc=0.15):
    copt = 0
    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    for c in global_svm_range:
        logging.info("Testing c value of {}".format(c))
        vectorizer = TfidfVectorizer(analyzer='char',
                                     ngram_range=(2, 4),
                                     max_features=max_features)
        vectorizer2 = TfidfVectorizer(ngram_range=(1, 3),
                                      max_features=max_features)

        features = [('word', vectorizer), ('char', vectorizer2)]
        clf = LinearSVC(C=c)
        svm_pip = pipeline.Pipeline([('union',
                                      FeatureUnion(transformer_list=features)),
                                     ('scale', Normalizer()),
                                     ('classifier', clf)])

        svm_pip.fit(train_sequences, train_targets)
        preds = svm_pip.predict(dev_sequences)
        if len(np.unique(train_targets)) > 2:
            average = "micro"
        else:
            average = "micro"
        f1 = f1_score(preds, dev_targets, average=average)
        if f1 > copt:
            logging.info("Improved performance to {}".format(f1))
            copt = f1

    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(2, 4),
                                 max_features=max_features)
    vectorizer2 = TfidfVectorizer(ngram_range=(1, 3),
                                  max_features=max_features)
    features = [('word', vectorizer), ('char', vectorizer2)]
    clf = LinearSVC(C=c)
    svm_pip = pipeline.Pipeline([('union',
                                  FeatureUnion(transformer_list=features)),
                                 ('scale', Normalizer()), ('classifier', clf)])
    return svm_pip.fit(train_sequences, train_targets)


def BASELINE_get_tpot_word_pipeline(train_sequences,
                                    train_targets,
                                    time_constraint=1,
                                    num_cpu=1,
                                    max_features=1000,
                                    dev_perc=0.15):

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    vectorizer = TfidfVectorizer(ngram_range=(1, 3), max_features=max_features)
    features = [('word', vectorizer)]
    clf = TPOTClassifier(generations=5,
                         population_size=50,
                         verbosity=2,
                         random_state=42,
                         config_dict = 'TPOT sparse')

    auml_pip = pipeline.Pipeline([('union',
                                   FeatureUnion(transformer_list=features)),
                                  ('scale', Normalizer()), ('tpot', clf)])

    sequence_space = train_sequences + dev_sequences

    Y_train = np.array(train_targets + dev_targets)
    auml_pip.fit(sequence_space, Y_train)
    return auml_pip


#def BASELINE_get_paraphrase_multilingual_mpnet_base_v2(train_sequences,
#                        train_targets,
#                        time_constraint=1,
#                        num_cpu=1,
#                        max_features=1000,
#                        dev_perc=0.15):
#
#    clf = LinearSVC(C=1)
#    model = SentenceTransformer('paraphrase-multilingual-mpnet-base-v2',
#                                device="cpu")
#    sentence_embeddings = model.encode(train_sequences, show_progress_bar=True)
#
#    return clf.fit(sentence_embeddings, train_targets), model

def BASELINE_get_all_mpnet_base_v2(train_sequences,
                        train_targets,
                        time_constraint=1,
                        num_cpu=1,
                        max_features=1000,
                        dev_perc=0.15):

    clf = LinearSVC(C=1)
    model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2',
                                device="cpu")
    sentence_embeddings = model.encode(train_sequences, show_progress_bar=True)

    return clf.fit(sentence_embeddings, train_targets), model


def BASELINE_get_majority(train_sequences,
                          train_targets,
                          time_constraint=1,
                          num_cpu=1,
                          max_features=1000,
                          dev_perc=0.15):
    copt = 0
    c = 0

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    max_features = 100
    logging.info("Testing c value of {}".format(c))
    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(1, 1),
                                 max_features=max_features)
    vectorizer2 = TfidfVectorizer(ngram_range=(1, 1),
                                  max_features=max_features)

    features = [('word', vectorizer), ('char', vectorizer2)]
    clf = DummyClassifier()
    svm_pip = pipeline.Pipeline([('union',
                                  FeatureUnion(transformer_list=features)),
                                 ('scale', Normalizer()), ('classifier', clf)])

    svm_pip.fit(train_sequences, train_targets)
    preds = svm_pip.predict(dev_sequences)
    if len(np.unique(train_targets)) > 2:
        average = "micro"
    else:
        average = "micro"
    f1 = f1_score(preds, dev_targets, average=average)
    if f1 > copt:
        logging.info("Improved performance to {}".format(f1))
        copt = f1

    vectorizer = TfidfVectorizer(analyzer='char',
                                 ngram_range=(1, 1),
                                 max_features=max_features)
    vectorizer2 = TfidfVectorizer(ngram_range=(1, 1),
                                  max_features=max_features)
    features = [('word', vectorizer), ('char', vectorizer2)]
    clf = DummyClassifier()
    svm_pip = pipeline.Pipeline([('union',
                                  FeatureUnion(transformer_list=features)),
                                 ('scale', Normalizer()), ('classifier', clf)])
    return svm_pip.fit(train_sequences, train_targets)


logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


def BASELINE_get_doc2vec(train_sequences,
                         train_targets,
                         time_constraint=1,
                         num_cpu=1,
                         max_features=1000,
                         classifier="LR",
                         dev_perc=0.15):

    whole_length = len(train_sequences)
    dev_num = int(whole_length * (1 - dev_perc))

    dev_sequences = train_sequences[dev_num:]
    dev_targets = train_targets[dev_num:]

    train_sequences = train_sequences[0:dev_num]
    train_targets = train_targets[0:dev_num]

    total_sequences_training = train_sequences + dev_sequences
    total_labels_training = train_targets + dev_targets
    if classifier == "LR":
        clf = LogisticRegression(max_iter=100000)
    else:
        clf = LinearSVC()
    documents = [
        TaggedDocument(simple_preprocess(doc), [i])
        for i, doc in enumerate(total_sequences_training)
    ]
    model = Doc2Vec(documents,
                    vector_size=512,
                    window=5,
                    min_count=2,
                    epochs=32,
                    num_cpu=8)
    vecs = []
    for doc in total_sequences_training:
        vector = model.infer_vector(simple_preprocess(doc))
        vecs.append(vector)
    train_matrix = np.matrix(vecs)
    clf.fit(train_matrix, total_labels_training)
    return (clf, model)


# def BASELINE_get_bert_base(train_sequences,
#                   train_targets,
#                   time_constraint=1,
#                   num_cpu=1,
#                   max_features=1000,
#                   model="bert-base",
#                   dev_perc = 0.15):

#     whole_length = len(train_sequences)
#     dev_num = int(whole_length * (1 - dev_perc))

#     dev_sequences = train_sequences[dev_num:]
#     dev_targets = train_targets[dev_num:]

#     train_sequences = train_sequences[0:dev_num]
#     train_targets = train_targets[0:dev_num]

#     'text' 'labels'
#     total_sequences_training = train_sequences + dev_sequences

#     total_labels_training = train_targets + dev_targets

#     train_df = pd.DataFrame()
#     train_df['text'] = total_sequences_training
#     train_df['labels'] = total_labels_training

#     # Create a ClassificationModel
#     model_args = {}
#     model_args['num_train_epochs'] = 3
#     model_args['max_sequence_length'] = 512
#     model_args['save_eval_checkpoints'] = False
#     model_args['save_model_every_epoch'] = False
#     #model_args['save_steps'] = 4000
#     model = ClassificationModel('bert',
#                                 'bert-base-multilingual-cased',
#                                 num_labels=len(set(total_labels_training)),
#                                 args = model_args,
#                                 use_cuda=False)

#     # elif model == "roberta-base":
#     #     model = ClassificationModel('roberta',
#     #                                 'roberta-base',
#     #                                 num_labels=len(set(total_labels_training)),
#     #                                 args={
#     #                                     'reprocess_input_data': True,
#     #                                     'overwrite_output_dir': True
#     #                                 },
#     #                                 use_cuda=True)

#     model.train_model(train_df)
#     return model
