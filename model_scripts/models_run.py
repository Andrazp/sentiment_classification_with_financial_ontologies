## a script which computes baselines' performance
from model_scripts.models import *
from cluster_utils import *
import secrets
import sys
from pathlib import Path
import pandas as pd
import argparse
import os

parser = argparse.ArgumentParser()    
parser.add_argument('--folder_name',default="random", type = str) 
parser.add_argument('--threshold',default="k10", type = str) 
parser.add_argument('--df',default="../data/oversampling_baselines_datasets/random_oversampled_train_k10.csv", type = str)
args = parser.parse_args()

Path("../predictions").mkdir(parents=True, exist_ok=True)
Path("../models").mkdir(parents=True, exist_ok=True)
Path("../baseline_reports").mkdir(parents=True, exist_ok=True)

if not os.path.exists(f"../predictions/"+args.folder_name+"_"+args.threshold):
    os.makedirs(f"../predictions/"+args.folder_name+"_"+args.threshold)

if __name__ == "__main__":

    all_baselines = dir(sys.modules[__name__])
    jid = secrets.token_hex(nbytes=16)
    df_path = args.df

    ## Load example data frame
    dataframe = pd.read_csv(df_path)
    final_sequences = dataframe.Sentences.values.tolist()

    subframe = dataframe[['Sentences', 'Sentiment']]
    subframe = subframe.replace('nan', np.NaN)
    subframe = subframe.dropna()
    train_sequences = subframe.Sentences.values.tolist()
    print(subframe.size)
    train_targets = subframe.Sentiment.values.tolist()

    for el in all_baselines:
        if "BASELINE" in el:
            bstring = el + "(train_sequences,train_targets)"
            bname = "-".join(el.split("_")[2:4])
            try:

                print(f"evaluation of {bname}")
                out_file = "../processed_data/test.csv"
                dfx = pd.read_csv(out_file)
                test_sequences = dfx.Sentences.values.tolist()
                print(dfx.size)
                baseline_model = eval(bstring)
                if type(baseline_model) == tuple:
                    if "doc2vec" in bname:
                        (clf, model) = baseline_model
                        vecs = []
                        for doc in test_sequences:
                            vector = model.infer_vector(simple_preprocess(doc))
                            vecs.append(vector)
                        test_sequences = vecs
                        baseline_model = clf

                    elif "mpnet" in bname:
                        (clf, model) = baseline_model
                        test_sequences = model.encode(test_sequences)
                        baseline_model = clf

                predictions = baseline_model.predict(test_sequences)
                # if "bert" in bname:
                #     predictions, raw_outputs = baseline_model.predict(test_sequences)

                test_classes = dfx.Sentiment.values.tolist()
                try:
                    predictions = predictions.tolist()
                except:
                    pass

                ## Output the results + predictions for further use
                
                output_classification_results(
                    predictions,
                    test_classes,
                    f"../predictions/"+args.folder_name+"_"+args.threshold+f"/test_{jid}_{bname}_report.json",
                    print_out=False)
                print(f"written: {bname}")
            except Exception as es:
                print(f"Could not evaluate baseline: {bname}, {es}")
                continue
