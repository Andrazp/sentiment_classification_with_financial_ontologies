## Create dataset where we lematize each sentence
import pandas as pd 
import string
import re
from lemmagen3 import Lemmatizer

df = pd.read_csv('../martin_additional_experiment/processed_data/test.csv', error_bad_lines=False)
lem_en = Lemmatizer('en')
sentences = []
targets = []
for index, row in df.iterrows():
    tokens = row['Sentences'].lower().split()
    sentence_lemmatized = ' '.join([lem_en.lemmatize(token) for token in tokens])
    sentences.append(sentence_lemmatized)
    targets.append(row['Sentiment'])
    

word_dataframe = pd.DataFrame({"Sentences": sentences, "Sentiment": targets})

word_dataframe.to_csv('../martin_additional_experiment/lemmatized_data/test.csv')