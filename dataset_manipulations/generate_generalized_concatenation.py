## add generalizations to dataset
import json
import pandas as pd
import wordninja as wn
import copy
import argparse
import re
from sklearn.model_selection import train_test_split

def contains_word(s, w):
    return (' ' + w + ' ') in (' ' + s + ' ')

def isfloat(str):
    try: 
        float(str)
    except ValueError: 
        return False
    return True

parser = argparse.ArgumentParser()    
parser.add_argument('--generalizations_file',default="0.1-threshold")

generalized_sentences = []
targets = []
counter = 0
count_rows = 0

df_ = pd.read_csv('../martin_additional_experiment/lemmatized_data/train.csv', error_bad_lines=False)
print("Data shape:")
print(df_.shape)
generalized_term_feature_list = []

args = parser.parse_args()

gen_dict = {}

with open("../generalizations/"+args.generalizations_file+".json", "r") as read_file:
    data_string = json.load(read_file)
    data = json.loads(data_string)

    for index, row in df_.iterrows():
        count_rows += 1
        row_sentence = row['Sentences']
        added_feature = ''
        for json_class in data['resulting_generalization']:
            if json_class != 'average_depth' and json_class != 'average_association' and not isfloat(json_class): ## go over the sentences and replace json_class with word_joined instances
                if contains_word(row_sentence, json_class):
                    if json_class in gen_dict.keys():
                        gen_dict[json_class] += 1
                    else:
                        gen_dict[json_class] = 1
                    texts = data['resulting_generalization'][json_class]['terms']
                    for text in texts:
                        last_word_split = text.split('/')
                        last_word = last_word_split[len(last_word_split) -1]
                        word_splitted = wn.split(last_word)
                        word_joined = ' '.join(word_splitted)
                        if word_joined != json_class:
                            counter += 1
                            if added_feature != '':
                                added_feature += ', ' + word_joined
                            else:
                                added_feature = word_joined
        generalized_term_feature_list.append(added_feature)

df_['Generalizations'] = generalized_term_feature_list
df_.to_csv('../martin_additional_experiment/generalized_data/train_concatenation.csv')  

print(sorted(gen_dict, key=gen_dict.get, reverse=True))

print("Added an average of: " + str(counter/count_rows))