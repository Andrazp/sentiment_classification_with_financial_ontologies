## Create dataset where each word in documents is it's own class
import pandas as pd 
import string
import re
  
def remove_numbers(list):
    pattern = '[0-9]'
    list = [re.sub(pattern, '', i) for i in list]
    return list

df = pd.read_csv('../processed_data/whole_dataset.csv', error_bad_lines=False)
words = ' '.join([i for i in df['Sentences']]).split()
words = [''.join(c for c in s if c not in string.punctuation) for s in words]
words = remove_numbers(words)
words =  [x.upper() for x in words]
words = list(set(words))
word_dataframe = pd.DataFrame({"word": words, "target": range(len(words))})

word_dataframe.to_csv('../generalized_data/word_data.csv')