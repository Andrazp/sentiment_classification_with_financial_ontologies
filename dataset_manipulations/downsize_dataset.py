## create 10% train set

import pandas as pd 
import string
import re
from lemmagen3 import Lemmatizer

df = pd.read_csv('../processed_data/train.csv', error_bad_lines=False)
positive_rows = df.loc[df['Sentiment'] == 1]
neutral_rows = df.loc[df['Sentiment'] == 0]
negative_rows = df.loc[df['Sentiment'] == -1]
positive_precent = positive_rows.sample(frac=0.1)
neutral_precent = neutral_rows.sample(frac=0.1)
negative_precent = negative_rows.sample(frac=0.1)

frames = [positive_precent, neutral_precent, negative_precent]
downsized_dataframe = pd.concat(frames)
downsized_dataframe.to_csv('../processed_data/downsized_train_data.csv')