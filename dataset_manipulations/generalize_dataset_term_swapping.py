## generalize dataset
import json
import pandas as pd
import wordninja as wn
import copy
import argparse
import re

def contains_word(s, w):
    return (' ' + w + ' ') in (' ' + s + ' ')

def isfloat(str):
    try: 
        float(str)
    except ValueError: 
        return False
    return True

parser = argparse.ArgumentParser()    
parser.add_argument('--augmentation_factor',default=10, type = float)
parser.add_argument('--generalizations_file',default="1-threshold")

generalized_sentences = []
targets = []


df_ = pd.read_csv('../martin_additional_experiment/lemmatized_data/train.csv', error_bad_lines=False)

print("Starting shape:")
print(df_.shape)
bag_of_sentences = df_['Sentences'].tolist()
bag_of_targets = df_['Sentiment'].tolist()

args = parser.parse_args()
max_len = args.augmentation_factor * len(bag_of_sentences)
print("Soft threshold for maximum length: " + str(max_len))

with open("../generalizations/" + args.generalizations_file + ".json", "r") as read_file:
    data_string = json.load(read_file)
    data = json.loads(data_string)
    iteration = 0
    for json_class in data['resulting_generalization']:
        if len(bag_of_sentences) > max_len:
            break
        if json_class != 'average_depth' and json_class != 'average_association' and not isfloat(json_class): ## go over the sentences and replace json_class with word_joined instances
            iteration += 1
            tmp_bag = []
            tmp_targets = []
            used_sentences = [0] * len(bag_of_sentences)
            texts = data['resulting_generalization'][json_class]['terms']
            for text in texts:
                last_word_split = text.split('/')
                last_word = last_word_split[len(last_word_split) -1]
                word_splitted = wn.split(last_word)
                word_joined = ' '.join(word_splitted)
                if word_joined != json_class:
                    for sentence_ix in range(len(bag_of_sentences)):
                        sentence = bag_of_sentences[sentence_ix]
                        if contains_word(sentence, json_class):
                            new_sentence = re.sub(' ' + json_class + ' ', ' ' + word_joined + ' ', ' ' + sentence + ' ')
                            new_sentence = new_sentence[1:-1]
                            tmp_bag.append(new_sentence)
                            tmp_targets.append(bag_of_targets[sentence_ix])
                            used_sentences[sentence_ix] = 1


            for ix in range(len(used_sentences)):
                if True: #used_sentences[ix] == 0: INCLUDE BOTH
                    tmp_bag.append(bag_of_sentences[ix]) ## INCLUDE NON GENERALIZED SENTENCES
                    tmp_targets.append(bag_of_targets[ix])

            bag_of_sentences = copy.deepcopy(tmp_bag)
            bag_of_targets = copy.deepcopy(tmp_targets)

generalized_dataframe = pd.DataFrame({"Sentences": bag_of_sentences, "Sentiment": bag_of_targets})
print("Resulting shape:")
print(generalized_dataframe.shape)
generalized_dataframe.to_csv('../martin_additional_experiment/generalized_data/train_k='+str(args.augmentation_factor)+'.csv')    
