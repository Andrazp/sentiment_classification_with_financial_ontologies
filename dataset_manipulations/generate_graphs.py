## generate report graphs
import json
import pandas as pd
import os
import matplotlib.pyplot as plt
from texttable import Texttable


learners = ["doc2vec", "lr-char", "lr-word", "svm-char", "svm-word", "tpot-word", "smbert"]
generalization_parameters = ["k-0", "g_feature_pure_test", "g_feature", "k-2", "k-5", "k-10"]
dict_of_dicts = {}  # name of learner then name of parameter then f1 score

## baselines without generalization
for generalization in generalization_parameters:
    for filename in os.listdir("../predictions/" + generalization):
        for learner in learners:
            if filename.endswith(learner + "_report.json"):
                file_p = open("../predictions/" + generalization + "/" + filename, "r")
                data = json.load(file_p)
                f1_score = data["macro avg"]["f1-score"]
                if learner not in dict_of_dicts:
                    dict_of_dicts[learner] = {}
                dict_of_dicts[learner][generalization] = f1_score

for key_a in dict_of_dicts.keys():

    keys = dict_of_dicts[key_a].keys()
    values = dict_of_dicts[key_a].values()

    plt.plot(keys, values, label = key_a)   

plt.xlabel("Augmentation factor")
plt.ylabel("F1-score")
plt.legend() 
plt.savefig("../graphs/all_performances")
plt.clf()
